from django.db import models
from django.db.models.deletion import SET_NULL


class Organization(models.Model):
    name = models.CharField(max_length=80)
    acronym = models.CharField(max_length=20, null=True, blank=True)

    foundation_date = models.DateField(null=True, blank=True)
    logo = models.ImageField(upload_to='organization_logos')

    ambit = models.ManyToManyField('Ambit')

    website = models.URLField(null=True, blank=True)
    magazine = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)
    related_links  = models.JSONField(null=True, blank=True)
    diffusion_organ = models.URLField(null=True, blank=True)

    youth = models.ForeignKey('Organization', on_delete=SET_NULL, blank=True, null=True, related_name='organization_youth')

    predecessor_organizations = models.ManyToManyField('Organization', blank=True)
    
    def __str__(self):
        return self.name


class CommunistParty(Organization):
    is_active = models.BooleanField()

    national_topic = models.TextField(null=True, blank=True)
    feminism_topic = models.TextField(null=True, blank=True)
    trans_topic = models.TextField(null=True, blank=True)
    ideological_current = models.TextField(null=True, blank=True)


class Ambit(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name


class Contact(models.Model):
    email = models.EmailField(verbose_name="E-mail", null=True)
    content = models.TextField(verbose_name="Sugerencia")
    def __str__(self):
        return self.content