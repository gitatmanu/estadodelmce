from bs4 import BeautifulSoup
from selenium import webdriver
from time import sleep
import random, os
from dotenv import load_dotenv
load_dotenv()

class InstagramScraper:
    @classmethod
    def init_driver(self):
        options = webdriver.FirefoxOptions()
        options.add_argument('--headless')
        profile = webdriver.FirefoxProfile()
        profile.set_preference('intl.accept_languages', 'en-GB')

        return webdriver.Firefox(firefox_profile=profile, options=options, executable_path='/home/manel/statistics_scraping_tools/geckodriver')

    @classmethod
    def login(self, driver):
        print('Logging in...')
        driver.get('https://instagram.com/')
        sleep(random.randint(3, 5))

        accept_cookies = driver.find_element_by_xpath("//button[text()='Accept All']")
        accept_cookies.click()

        username_input = driver.find_element_by_xpath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[1]/div/label/input")
        password_input = driver.find_element_by_xpath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[2]/div/label/input")
        username_input.send_keys(os.getenv('INSTAGRAM_USER'))
        password_input.send_keys(os.getenv('INSTAGRAM_PASSWORD'))
        sleep(2)

        login_button = driver.find_element_by_xpath('//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div/div[3]/button')
        login_button.click()
        sleep(5)

        # Save information modal
        try:
            not_now_button = driver.find_element_by_xpath("/html/body/div[1]/section/main/div/div/div/div/button")
            not_now_button.click()
            sleep(3)
        except Exception:
            print('Save information modal not found')
            pass
    
        # Notifications modal
        try:
            no_notifications = driver.find_element_by_class_name("aOOlW.HoLwm")
            no_notifications.click()
            sleep(3)
        except Exception:
            print('Notifications modal not found')
            pass
        
        print('Logged')
        return driver

    @classmethod
    def scrap_account(self, driver, nickname):
        driver.get('https://instagram.com/' + nickname)
        sleep(2)

        bs = BeautifulSoup(driver.page_source,'lxml')

        try:
            followers = bs.find('a', attrs={"href": "/" + nickname + "/followers/"}).find('span').get('title')
        except:
            print('Account name not found...')
            return {}

        return {"nickname": nickname,
                "followers": followers}

class TwitterScraper:
    @classmethod
    def init_driver(self):
        options = webdriver.FirefoxOptions()
        options.add_argument('--headless')
        profile = webdriver.FirefoxProfile()
        profile.set_preference('intl.accept_languages', 'en-GB')

        return webdriver.Firefox(firefox_profile=profile, options=options, executable_path='/home/manel/statistics_scraping_tools/geckodriver')

    @classmethod
    def format_twitter_followers(self, followers):
        followers = followers.replace(',', '').replace('.', '')
        followers = followers.replace('K', '00').replace('M', '00000')

        return int(followers)
    
    @classmethod
    def scrap_account(self, driver, nickname):
        driver.get('https://twitter.com/' + nickname)
        sleep(2)

        bs = BeautifulSoup(driver.page_source,'html.parser')
        try:
            followers = bs.find('a', attrs={"href": "/" + nickname +"/followers"}).find('span').find('span').string.replace(u'\xa0', u' ')
            followers = TwitterScraper.format_twitter_followers(followers)
        except:
            print('Account name not found...')
            return {}

        return {"nickname": nickname,
                "followers": followers}