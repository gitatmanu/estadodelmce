from django.contrib import admin
from .models import Organization, CommunistParty, Contact 

class ContactAdmin(admin.ModelAdmin):
    list_display = ['email', 'content']

class CommunistPartyAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_active', 'facebook']
    list_editable = ['facebook']

admin.site.register(Organization)
admin.site.register(CommunistParty, CommunistPartyAdmin)
admin.site.register(Contact, ContactAdmin)
