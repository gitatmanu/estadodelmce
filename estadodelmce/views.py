from django.views.generic import FormView, TemplateView
from estadodelmce.models import CommunistParty, Contact
from estadodelmce.forms import ContactForm
from django.http import JsonResponse
import ast
from django.conf import settings
from django.core.mail import send_mail

class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['objects'] = CommunistParty.objects.all()
        return context

def ajax_contact(request):
    content = ast.literal_eval(request.body.decode("UTF-8"))['content']
    email = ast.literal_eval(request.body.decode("UTF-8"))['email']

    contact = Contact()
    if content:
        contact.content = content
    
    if email:
        contact.email = email

    contact.save()

        #send_mail(
        #   'Nueva sugerencia en estadodelmce.es',
        #    str(content),
        #    settings.EMAIL_HOST_USER,
        #    ['manumcraft@gmail.com'],
        #    fail_silently=False
        #    )

    return JsonResponse({})


class CommunistPartiesView(TemplateView):
    template_name = "communistparty_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['objects'] = CommunistParty.objects.all().order_by('name')
        return context
